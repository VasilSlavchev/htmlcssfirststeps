<?PHP
    $directory  = getcwd();
    print '<p><strong>Current Directory:  ';
    print_r($directory).'<br>';
    $dir  = getcwd();
    $directoryScan = scandir($directory);

    function getFileList($dir)
    {
        // array to hold return value
        $retval = array();

        // add trailing slash if missing
        if(substr($dir, -1) != "/") $dir .= "/";

        // open pointer to directory and read list of files
        $d = @dir($dir) or die("Function /getFileList: Failed opening directory $dir for reading");
        while(false !== ($entry = $d->read())) {
        // skip hidden files
            if($entry[0] == ".") continue;
            if(is_dir("$dir$entry")) {
                $retval[] = array(
                                  "name" => "$dir$entry/",
                                  "type" => filetype("$dir$entry"),
                                  "size" => 0,
                                  "lastmod" => filemtime("$dir$entry")
                                  );
            } elseif(is_readable("$dir$entry")) {
                $retval[] = array(
                                  "name" => "$dir$entry",
                                  // "type" => mime_content_type("$dir$entry"),
                                  // "name" => basename("$dir$entry"),
                                  "type" => filetype("$dir$entry"),
                                  "size" => filesize("$dir$entry"),
                                  "lastmod" => filemtime("$dir$entry")
                                  );
            }
        }
        $d->close();

        return $retval;
    }
    // getFileList($dir);

    function _mime_content_type($filename) {
    $result = new finfo();

    if (is_resource($result) === true) {
        return $result->file($filename, FILEINFO_MIME_TYPE);
    }

    return false;
}

    // list files in the current directory
      // $dirlist = getFileList("/");
      $dirlist = getFileList($directory);

      // $dirlist = getFileList("Bootstrap");

      // $dirlist = getFileList("./Bootstrap");
      // $dirlist = getFileList("../Bootstrap");

      // using an absolute path
      // $dirlist = getFileList("{$_SERVER['DOCUMENT_ROOT']}");
      // $dirlist = getFileList("{$_SERVER['DOCUMENT_ROOT']}/Bootstrap");

        // echo "<pre>",print_r($dirlist),"</pre>";

          // output file list in HTML TABLE format
          echo "<table border=\"1\">\n";
          echo "<thead>\n";
          echo "<tr><th>Name</th><th>Type</th><th>Size</th><th>Last Modified</th></tr>\n";
          echo "</thead>\n";
          echo "<tbody>\n";
          foreach($dirlist as $file) {
            echo "<tr>\n";
            echo "<td><a href=\"$directory.{$file['name']}\">{$file['name']}</a></td>\n";
            echo "<td>{$file['type']}</td>\n";
            echo "<td>{$file['size']}</td>\n";
            echo "<td>",date('r', $file['lastmod']),"</td>\n";
            echo "</tr>\n";
          }
          echo "</tbody>\n";
          echo "</table>\n\n";

          echo "<pre>";
          print_r($directoryScan);
          echo "</pre>";
?>